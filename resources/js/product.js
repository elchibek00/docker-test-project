$(document).ready(function() {
    let i = 0;
    let count = $('#count-data').val();
    if (count > 0) {
        i = count;
    }

    $('.add-data').click(function() {
        let newData =
            '<div class="data-attributes"><div class="col-md-4"><label for="name">Название: </label>' +
            '<input type="text" name="data[' + i + '][key]" class="form-control">' +
            '</div>' +
            '<div class="col-md-4"><label for="name">Значение: </label>' +
            '<input type="text" name="data[' + i + '][value]" class="form-control">' +
            '</div><button type="button" class="btn btn-danger mt-2 remove-data">Удалить атрибут</button>';
        $('.dynamic-data').append(newData);

        i++;
    });

    $('.dynamic-data').on('click', '.remove-data', function() {
        $(this).parent().remove();
    });
});
