@extends('layouts.app')

@section('content')
    <h1>Добавить продукт</h1>
    <form action="{{route('products.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Артикул</label>
            <input type="text" class="form-control form-control-sm col-6" name="article" required>
            @error('article')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="name">Название</label>
            <input type="text" class="form-control form-control-sm col-6" name="name" required>
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="name">Статус</label>
            <select class="form-control form-control-sm col-6" name="status">
                @foreach($statuses as $status)
                    <option value="{{$status}}">{{$status}}</option>
                @endforeach
            </select>
            @error('status')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="data">Атрибуты</label>
            <div class="form-group dynamic-data">
            </div>
            <button type="button" class="btn btn-primary mt-2 add-data">Добавить атрибут</button>
        </div>
        <div class="form-group mt-5">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
