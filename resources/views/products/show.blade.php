@extends('layouts.app')
@section('content')
    <h1>{{$product->name}}</h1>
    <div class="form-group">
        <label for="name">Артикул</label>
        <h3>{{$product->article}}</h3>
    </div>
    <div class="form-group">
        <label for="name">Название</label>
        <h3>{{$product->name}}</h3>
    </div>
    <div class="form-group">
        <label for="name">Статус</label>
        <h3>{{$product->status}}</h3>
    </div>
    <div class="form-group">
        <label for="data">Атрибуты</label>
        @if($countData > 0)
            @foreach($product->data as $attribute)
                <div class="row">
                    <div class="col-md-4">
                        <label for="name">Именование: </label>
                        <h4>{{ $attribute['key'] ?? '' }}</h4>
                    </div>
                    <div class="col-md-4">
                        <label for="name">Значение: </label>
                        <h4>{{ $attribute['value'] ?? '' }}</h4>
                    </div>
                </div>
            @endforeach
        @endif
        <div class="form-group dynamic-data">
        </div>
    </div>
    <button class="btn btn-info"><a href="{{route('products.index')}}">Назад</a></button>
    <button class="btn btn-warning"><a href="{{route('products.edit', ['product' => $product])}}">Изменить</a></button>
@endsection
