@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="ml-auto text-end">
            <button class="btn btn-primary"><a href="{{ route('products.create') }}" style="color: #fff;">Добавить</a></button>
        </div>
        <div class="row mt-5 pb-5">
            <form action="{{ route('products.restore-all') }}" method="post">
                @csrf
                @method('POST')
                <button class="btn btn-warning mb-3">Восстановить все архивированные продукты</button>
            </form>
            <div class="col-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col" style="width: 5%;">#</th>
                        <th scope="col" style="width: 15%;">Артикул</th>
                        <th scope="col" style="width: 15%;">Название</th>
                        <th scope="col" style="width: 10%;">Статус</th>
                        <th scope="col" style="width: 25%;">Атрибуты</th>
                        <th scope="col" style="width: 35%;">Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $product->article }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->status }}</td>
                            <td>
                                @if($product->data)
                                    @foreach($product->data as $property)
                                        <b>{{ $property['key'] }}</b>: {{ $property['value'] }}&nbsp;&nbsp;
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Product Actions">
                                    @if(empty($product->deleted_at))
                                        <button class="btn btn-warning" style="margin-right: 5px;">
                                            <a href="{{ route('products.edit', ['product' => $product]) }}" style="color: #fff;">Изменить</a>
                                        </button>
                                    @else
                                        <form action="{{ route('products.restore', ['id' => $product->id]) }}" method="post" style="margin-right: 5px;">
                                            @method('post')
                                            @csrf
                                            <button class="btn btn-success">Восстановить</button>
                                        </form>
                                    @endif

                                    @if(empty($product->deleted_at))
                                        <form action="{{ route('products.destroy', ['product' => $product]) }}" method="post" style="margin-right: 5px;">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger">Архивировать</button>
                                        </form>
                                    @endif

                                    <form action="{{ route('products.force-delete', ['id' => $product->id]) }}" method="post" style="margin-right: 5px;">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger">Удалить</button>
                                    </form>

                                    <button class="btn btn-info">
                                        <a href="{{ route('products.show', ['product' => $product]) }}" style="color: #fff;">Просмотреть</a>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
