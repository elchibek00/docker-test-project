@extends('layouts.app')
@section('content')
    <h1>Изменить продукт</h1>
    <form action="{{route('products.update', ['product' => $product])}}" method="post">
        @csrf
        @method('PUT')
        @can('edit product-article')
            <div class="form-group">
                <label for="name">Артикул</label>
                <input type="text" class="form-control form-control-sm col-6" name="article" required value="{{$product->article}}">
                @error('article')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        @endcan
        <div class="form-group mt-3">
            <label for="name">Название</label>
            <input type="text" class="form-control form-control-sm col-6" name="name" required value="{{$product->name}}">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="name">Статус</label>
            <select name="status">
                @foreach($statuses as $status)
                    <option value="{{$status}}">{{$status}}</option>
                @endforeach
            </select>
            @error('status')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="data">Атрибуты</label>
            @if($countData > 0)
                @for($i = 0; $i < $countData; $i++)
                    <div class="row">
                        <div class="col-md-4">
                            <label for="name">Название: </label>
                            <input type="text" name="data[{{ $i }}][key]" class="form-control"
                                   value="{{ $product->data[$i]['key'] ?? '' }}">
                        </div>
                        <div class="col-md-4">
                            <label for="name">Значение: </label>
                            <input type="text" name="data[{{ $i }}][value]" class="form-control"
                                   value="{{ $product->data[$i]['value'] ?? '' }}">
                        </div>
                    </div>
                @endfor
            @endif
            <div class="form-group dynamic-data">
            </div>
            <input type="hidden" value="{{$countData}}" id="count-data" >
            <button type="button" class="btn btn-primary mt-3 add-data">Добавить атрибут</button>
        </div>
        <div class="form-group mt-4">
            <button type="submit" class="btn btn-primary">Изменить</button>
        </div>
    </form>
@endsection
