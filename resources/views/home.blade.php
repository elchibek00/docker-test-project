<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test App</title>
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body class="body-home">
<div class="container container-home">
    <h1 class="h1-home">Добро пожаловать!</h1>
    <p class="p-home">Пожалуйста, зарегистрируйтесь или войдите в систему.</p>
    <a href="{{route('register')}}" class="btn-home">Зарегистрироваться</a>
    <a href="{{route('login')}}" class="btn-home">Войти</a>
</div>
</body>
</html>
