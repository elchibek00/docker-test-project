<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
@if (session('message'))
    <div class="alert alert-primary" role="alert">
        {{session('message')}}
    </div>
@endif
<header>
    <h1>Админ Панель</h1>
</header>
<section>
    <aside>
        <a class="products" href="{{route('products.index')}}">Продукты</a>
    </aside>

    <main class="body">
        @yield('content')
    </main>
</section>
</body>
</html>
