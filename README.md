## Установка и конфигурация

Скачаиваем:

git clone git@gitlab.com:elchibek00/docker-test-project.git

После установки:

cp .env.example .env </br>
composer install </br>
php artisan key:generate </br>
npm install

## Запускаем проект
Убедитесь что предварительно скачали докер 

docker-compose up -d (ждем пока все не сбилдится) </br>
docker exec -it app bash </br>
php artisan migrate --seed (ждем пока все замигрирует) </br>

npm run dev или npm run build

## Тестим

заходим на страницу в браузере localhost:8000 </br>

регистрируемся или логинимся(уже есть базовый админ ) </br>

и теперь можно работать !




