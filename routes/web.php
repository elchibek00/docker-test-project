<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
   return view('home');
});

Route::middleware(['auth'])->group(function () {

    Route::resource('products', ProductController::class)
        ->middleware(['role:user|admin']);

    Route::delete('products/{id}/force-delete', [ProductController::class, 'forceDelete'])
        ->middleware(['role:admin'])
        ->name('products.force-delete');

    Route::post('products/{id}/restore', [ProductController::class, 'restore'])
        ->middleware(['role:admin'])
        ->name('products.restore');

    Route::post('products/restore-all', [ProductController::class, 'restoreAll'])
        ->middleware(['role:admin'])
        ->name('products.restore-all');
});

Auth::routes();
