<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create(['name' => 'admin'])
            ->syncPermissions(
                Permission::create(['name' => 'create product']),
                Permission::create(['name' => 'edit product']),
                Permission::create(['name' => 'delete product']),
                Permission::create(['name' => 'restore product']),
                Permission::create(['name' => 'show product']),
                Permission::create(['name' => 'destroy product']),
                Permission::create(['name' => 'restoreAll product']),
                Permission::create(['name' => 'edit product-article'])
            );

        Role::create(['name' => 'user'])->syncPermissions(
            Permission::where('name', 'create product'),
            Permission::where('name', 'edit product'),
            Permission::where('name', 'destroy product'),
            Permission::where('name', 'show product'),
        );
    }
}
