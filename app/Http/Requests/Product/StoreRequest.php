<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'article' => ['bail', 'required', 'unique:products'],
            'name' => ['bail', 'required', 'min:10', 'max:256', 'regex:/^[a-zA-Z0-9]+$/'],
            'status' => ['bail', 'required'],
            'data' => ['bail', 'nullable']
        ];
    }

    public function messages(): array
    {
        return [
            'article.required' => 'Артикль обязателен к заполнению',
            'name.required' => 'Название обязателен к заполнению',
            'name.min' => 'Минимальное значение 10 символов',
            'name.max' => 'Максимальное значение 256 символов',
            'name:unique' => '',
            'status.required' => 'Статус обязателен к заполнению',
            'data.bail' => 'Данные',
        ];
    }
}
