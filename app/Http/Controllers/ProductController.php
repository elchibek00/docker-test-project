<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\StoreRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::withTrashed()->status("available")->get();

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $statuses = collect(["available", "unavailable"]);

        return view('products.create', compact('statuses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        Product::create($request->validated());

        return redirect()->route('products.index')->with('message', "Продукт успешно создан!");
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        $countData = 0;
        if (!empty($product->data)){
            $countData = count($product->data);
        }
        return view('products.show', compact('product', 'countData'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        $statuses = collect(["available", "unavailable"]);
        $countData = 0;
        if (!empty($product->data)){
            $countData = count($product->data);
        }
        return view('products.edit', compact('product', 'statuses', 'countData'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->validate([
            'article' => ['bail', 'required', 'unique:products,article,' . $product->id],
            'name' => ['bail', 'required', 'min:10', 'max:256', 'regex:/^[a-zA-Z0-9]+$/'],
            'status' => ['bail', 'required'],
            'data' => ['bail']
        ]);

        $product->update($data);

        return redirect()->route('products.index')->with('message', "Продукт успешно изменен!");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')->with('message', "Продукт успешно архивирован!");
    }

    public function forceDelete($id)
    {
        Product::where('id', $id)->withTrashed()->forceDelete();

        return redirect()->route('products.index')->with('message', "Продукт успешно удален!");
    }

    public function restore($id)
    {
        Product::where('id', $id)->withTrashed()->restore();

        return redirect()->route('products.index')->with('message', "Продукт успешно восстановлен!");
    }

    public function restoreAll()
    {
        Product::onlyTrashed()->restore();

        return redirect()->route('products.index')->with('message', "Продукты успешно восстановлены!");
    }
}
