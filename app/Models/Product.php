<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $casts = [
        'data' => 'array'
    ];

    protected $dates = ['deleted_at'];

    protected $fillable = ['article', 'name', 'status', 'data'];

    public function scopeStatus($query, $value)
    {
        return $query->where('status', $value);
    }

    public function setDataAttribute($value): void
    {
        $data = [];

        foreach ($value as $array_item){
            if(!is_null($array_item['key'])){
                $data[] = $array_item;
            }
        }

        $this->attributes['data'] = json_encode($data);
    }
}
